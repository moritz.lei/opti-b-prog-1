// Info:
// Gesucht: Algorithmus der minimalen Spannbaum eines Graphen findet.
// 1. Möglichkeit: Prim
// 2. Möglichkeit: Kruskal

import java.util.*;

// Package org.jgrapht.graph:
// Link: https://jgrapht.org/javadoc/org.jgrapht.core/org/jgrapht/graph/package-summary.html

import org.jgrapht.Graph;
// public interface Graph<V, E>
/* 	From: https://github.com/jgrapht/jgrapht/blob/master/jgrapht-core/src/main/java/org/jgrapht/Graph.java

	Returns a Set of all edges connecting sourc vertex to target vertex in this graph. If 		any of the vertices does not exist or is <code>null</code>, returns 
	  <code>null</code>. If both vertices exist but no edges found, returns an empty set.

	  In undirected graphs, some of the returned edges may have their source and target 		vertices in the opposite order. In simple graphs the returned set is either 			singleton set or empty set.
*/
import org.jgrapht.graph.AsSubgraph;
import org.jgrapht.graph.DefaultWeightedEdge;

public class mySpanningTree {
	
	//Receives a graph and computes a minimum spanning tree
	public static AsSubgraph<Integer, DefaultWeightedEdge> computeMST(Graph<Integer, DefaultWeightedEdge> graph) {
		AsSubgraph<Integer, DefaultWeightedEdge> tree = new AsSubgraph<Integer, DefaultWeightedEdge>(graph, graph.vertexSet(), new HashSet<DefaultWeightedEdge>());
		return primAlgorithm(tree, graph);
	}

	public static AsSubgraph<Integer, DefaultWeightedEdge> primAlgorithm(AsSubgraph<Integer, DefaultWeightedEdge> tree, Graph<Integer, DefaultWeightedEdge> graph) {

		HashSet<Integer> vertices = new HashSet<>();
		vertices.add(1);
		ArrayList<DefaultWeightedEdge> edges = new ArrayList<>(graph.edgeSet());
		// passes our graph's getEdgeWeight(E e) method to comparingDouble, which returns a Comparator on DefaultWeightedEdge
		Comparator<DefaultWeightedEdge> edgeComparator = Comparator.comparingDouble(graph::getEdgeWeight);
		// O(|E|*log(|E|))
		Collections.sort(edges,edgeComparator);

		//O(|V|*|E|)
		while (vertices.size() < graph.vertexSet().size()) {
			for (DefaultWeightedEdge e : edges) {
				int s = graph.getEdgeSource(e);
				int t = graph.getEdgeTarget(e);
				if (vertices.contains(s) && !vertices.contains(t) || !vertices.contains(s) && vertices.contains(t)) {
					vertices.add(s);
					vertices.add(t);
					tree.addEdge(s, t);
					tree.setEdgeWeight(s, t, graph.getEdgeWeight(e));
					break;
				}
			}
		}

		return tree;
	}

}
