import java.util.*;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

//Implements shortest path algorithms. Can return distances to vertices from a given start vertex and the corresponding shortest path.
public class myShortestPath {
	//Variables
	private Graph<Integer, DefaultWeightedEdge> graph;			//The graph in which we search for a shortest path
	private Integer startVertex;								//The start vertex
	private HashMap<Integer, Double> distances;					//Distances from each vertex to the start vertex
	private HashMap<Integer, Integer> predecessors;				//Predecessors for each vertex on a shortest path to the start vertex
	
	//Constructor
	public myShortestPath(Graph<Integer, DefaultWeightedEdge> graph, Integer startVertex) {
		this.graph = graph;
		this.startVertex = startVertex;
		distances = new HashMap<Integer, Double>();
		predecessors = new HashMap<Integer, Integer>();
	}
	
	//Computes distances and predecessors for all nodes
	public void computeDistPred() {
		HashSet<Integer> setVertices = new HashSet<Integer>();
		setVertices.add(startVertex);
		distances.put(-1, Double.MAX_VALUE);
		distances.put(startVertex, 0.);
		int act = startVertex;
		// O(|V|^2)
		while(act != -1) {
			Set<DefaultWeightedEdge> outgoingEdges = graph.outgoingEdgesOf(act);
			for (DefaultWeightedEdge e : outgoingEdges) {														// Iteriere über alle ausgehenden Kanten des betrachteten Knoten.
				int target = graph.getEdgeTarget(e) == act ? graph.getEdgeSource(e) : graph.getEdgeTarget(e);	// Drehe Kante nötigenfalls um.
				double totalWeight = distances.get(act) + graph.getEdgeWeight(e);
				if (!distances.containsKey(target) || totalWeight < distances.get(target)) {      				// Update Distanz und Vorgänger, wenn ein neuer kürzester Weg vorliegt.
					distances.put(target, totalWeight);
					predecessors.put(target, act);
				}
			}
			act = -1;
			for (Map.Entry<Integer,Double> v : distances.entrySet()) {                                          // finde nächsten Knoten
				if (!setVertices.contains(v.getKey()) && v.getValue() < distances.get(act)) {
					act = v.getKey();
				}
			}
			setVertices.add(act);
		}
	}
	
	//Constructs the shortest path from the start node to a given end node using the list of predecessors
	public ArrayList<Integer> constructPathToNode(Integer endVertex) {
		ArrayList<Integer> path = new ArrayList<Integer>();
		path.add(endVertex);
		// O(|V|)
		while (!path.get(0).equals(startVertex)) {
			path.add(0,predecessors.get(path.get(0)));
		}
		return path;
	}
		
	//Returns the distance to a given end node
	public double getDistanceToNode(Integer endVertex) {
		return distances.get(endVertex);
	}
}
